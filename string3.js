
var months = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

function dateFormating(D) {
  var t = new Date(D);
  return months[t.getMonth()] + ' ' +  t.getDate();
}

exports.dateFormating = dateFormating;
